# we have not implement any sender yet, we use a mock 
# sender to test that the objects are well connected
sender = [MockSender()]

# we also use . the mock event for the same purpose
events = [MockEvent(senders)]

# an app instance is created an run. the mock event is
# the only event connected to it so far.
app = 