class APP:
    def __init__ (self, events):
        self.events = events
    def start (self):
        for event in self.events:
            event.check()
            